---
bookCollapseSection: true
weight: 3
---

# Enroll Mac OS device manually

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Go to devicemgt portal and click on Add device (https://IP:port/devicemgt/device/enroll)</li>
    <li>Click on iOS from "DEVICE TYPES"</li>
    <li>Type https://{IP}:{port}/ios-web-agent/enrollment in safari browser.</li>
</ul>
{{< /hint >}}


<iframe width="560" height="315" src="https://www.youtube.com/embed/5q478-OIWGE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Copy the url of QR code to the browser of the mac OS device.</li>
    <li>Click "Enroll without agent"</li>
    <li>Click "Install IoT Server Certificate"</li>
    <li>Open "Key Chain Acess" Application</li>
    <li>Open downloaded IoT Server Certificate</li>
    <li>Set trust to "Always trust" for the downloaded IoT Server Certificate.</li>
    <li>Click "Next"</li>
    <li>Type the Username: admin, Password: admin then click "sign in"</li>
    <li>Accept the Licence Agreement to continue.</li>
    <li>Then User will be promted to confirm the installation of the profile to the device. Then click "Install" to install the profile to the device.</li>
    <li>Then User will be promted to confirm the installation of the Mobile Device Management profile Then click "Install" to install the Mobile Device Management profile to the device
    </li>
</ul>
