# Integrating a Third-Party Identity Provider for Access Token Management

Entgra IoT Server is prepackaged with a default key manager to handle clients, security and access token-related operations. The following section provides an alternative to using the default key manager and guides you through using WSO2 Identity Server (WSO2 IS) as the key manger in Entgra IoT Server. This is useful in a production environment where a WSO2 IS node is already in use and you want to use the Entgra IoT Server with it. 



Before you begin



Before you begin



*   [Download WSO2 API Manager](https://wso2.com/api-management/#download) and unzip it if you have not done it before. The unzipped directory is referred to as `<APIM_HOME>` throughout this documentation.
*   [Download WSO2 Identity Server](https://wso2.com/identity-and-access-management#download) and unzip it. The unzipped directory is referred to as `<IS_HOME>` throughout this documentation.







1.  Follow the steps listed in the [Configuring WSO2 Identity Server as a Key Manager](https://docs.wso2.com/display/AM210/Configuring+WSO2+Identity+Server+as+a+Key+Manager) topic in the WSO2 API-M documentation, to configure WSO2 IS as the key manager. 
2.  Download the following [feature installation pom file](attachments/352822861/352822875.xml) and place it inside the `<APIM_HOME>` directory.

3.  Navigate to `<APIM_HOME>` on the terminal and execute the following command. This will install the required features to the Entgra IoT Server pack. 

    `mvn clean install -f apim-feature-installation.xml`

4.  Open the `identity.xml` file found in the `<APIM_HOME>/repository/conf/identity` directory and add the following grant type under the `<SupportedGrantTypes>` element. 

    
    <SupportedGrantType>
        <GrantTypeName>urn:ietf:params:oauth:grant-type:jwt-bearer</GrantTypeName>
        <GrantTypeHandlerImplClass>org.wso2.carbon.device.mgt.oauth.extensions.handlers.grant.ExtendedJWTGrantHandler</GrantTypeHandlerImplClass>
        <GrantTypeValidatorImplClass>org.wso2.carbon.identity.oauth2.grant.jwt.JWTGrantValidator</GrantTypeValidatorImplClass>
    </SupportedGrantType>

5.  Update the `OAuthScopeValidator` class as follows.

    `<OAuthScopeValidator class="org.wso2.carbon.device.mgt.oauth.extensions.handlers.ScopeValidationHandler"/>`

6.  The default keystore used in Entgra IoT Server has been updated from version 3.1.0 onwards. Since all servers must use the same certificate, do the following to update the keystore in WSO2 API-M

    1.  Navigate to the `<IOTS_HOME>/repository/resources/security` folder and copy the `wso2carbon.jks` file. 
    2.  Paste a copy of the file in the `<APIM_HOME>/repository/resources/security` folder and replace the existing `wso2carbon.jks` file. 
    3.  Paste a copy of the file in the `<IS_HOME>/repository/resources/security` folder as well and replace the existing `wso2carbon.jks` file. 
7.  Create a file based identity provider using the Entgra IoT Server certificate and save it inside the `<APIM_HOME>/repository/conf/identity/identity-providers` directory. A sample of a file based identity provider configuration file can be seen below. 

    

    

    The default IoT Server certificate can be found under the `<Certificate>` tag of the `iot-default.xml` file found in the `<IOTS_HOME>/repository/conf/identity/identity-providers` folder.

    

    

    

    

    

    

    <!--
      ~ Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
      ~
      ~ WSO2 Inc. licenses this file to you under the Apache License,
      ~ Version 2.0 (the "License"); you may not use this file except
      ~ in compliance with the License.
      ~ You may obtain a copy of the License at
      ~
      ~ http://www.apache.org/licenses/LICENSE-2.0
      ~
      ~ Unless required by applicable law or agreed to in writing,
      ~ software distributed under the License is distributed on an
      ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
      ~ KIND, either express or implied. See the License for the
      ~ specific language governing permissions and limitations
      ~ under the License.
      -->

    <IdentityProvider>
    	<IdentityProviderName>wso2.org/products/iot</IdentityProviderName>
    	<DisplayName>wso2.org/products/iot</DisplayName>
    	<IdentityProviderDescription></IdentityProviderDescription>
    	<Alias>devicemgt</Alias>
    	<IsPrimary>true</IsPrimary>
    	<IsFederationHub></IsFederationHub>
    	<HomeRealmId></HomeRealmId>
    	<ProvisioningRole></ProvisioningRole>
    	<FederatedAuthenticatorConfigs></FederatedAuthenticatorConfigs>
    	<DefaultAuthenticatorConfig>
    	</DefaultAuthenticatorConfig>
    	<ProvisioningConnectorConfigs>
    		<!--<ProvisioningConnectorConfig>
    			<ProvisioningProperties>
    			</ProvisioningProperties>
    		</ProvisioningConnectorConfig>-->
    	</ProvisioningConnectorConfigs>
    	<!--<DefaultProvisioningConnectorConfig></DefaultProvisioningConnectorConfig>-->
    	<ClaimConfig></ClaimConfig>
    	<Certificate>MIIDSTCCAjGgAwIBAgIERUubMzANBgkqhkiG9w0BAQsFADBVMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxDTALBgNVBAoTBFdTTzIxEjAQBgNVBAMTCWxvY2FsaG9zdDAeFw0xNzAzMjEwOTEzMDdaFw0xNzA2MTkwOTEzMDdaMFUxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzENMAsGA1UEChMEV1NPMjESMBAGA1UEAxMJbG9jYWxob3N0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu8lfLCQV2hbiz8OGA8baKI1E1cgE/QLKefa4Od2G2KextaAwSESr5ICakVX8w6tU5+IhKYQQKt9U3/U0ae9tXf04g6iWh0dRllsr9mOgjSZxQK09/ygUsFH8syL5aD3heRUYzJWZ/KOsd57BXuI2QZnuEjN0g0+5jNvnswfwD/tM01totaJpI3xN+2JZsaBRCc0G5yA/pdW83Aa4IE30EL57wkK7u8YDl3UTcmi+HO74XQmK1VEqSnA+/mFo3M16cRlm2PTZ2Z1E5gd4j+5sV1P5v63uqyFniEU0zPXforBb06pgSBoIRQBintSSDyEmyuVzW0pc2eYC5Dhfgk337QIDAQABoyEwHzAdBgNVHQ4EFgQU4lneZCvKn04NH5DtJItdRXdXankwDQYJKoZIhvcNAQELBQADggEBAEnCNr0JEcAxtF/ypwGgFu1rYOv3i02PB4jDWbFj8s58l1zF1tMr480khVX4d3AmLjsUpvV+Jg83JFIk1rJFLEb48EFv0nJ/G7pTQrCypNKozyuyLzx5UvKNFBMmnGvkHVqt6j4MSiOjnRdlo7ofDluA/umSkSf/VmsOIZ+5610pCJpahnPUkb8GYK0HcwNV2NLU/0X4nSjKjGwWYv0tX8XW8RwJKb/r0GDehA8PESi76XOVrpXSjP9WPK8XaK//8B0SH3hm0xpzmSQYgtkNQwP2MqBe/ZEUuKxrn3pP6oxo9RxeSIQ8dTLiBA/mcsBmAlpQPPi0LqqDKpkyHt8Ar2w=</Certificate>
    	<PermissionAndRoleConfig></PermissionAndRoleConfig>
    	<JustInTimeProvisioningConfig></JustInTimeProvisioningConfig>
    </IdentityProvider>

