---
weight: 1
---
# Enroll Agent using QR code

The following sections describe the setup that the system administrator needs to carry out in order to complete the Android device registration via QR code on Entgra IoT Server.

## Installing the agent

There are number of different way an Android device can be enrolled on to the IoT server. This mainly depends on if the device is a BYOD device or a COPE device. Since the BYOD devices are owned by the user that brings it in, the level of control and information retrieval from the device has to be done minimum. But the COPE devices are organization owned and it is possible to execute any type of commons on top of these devices. This distinction of access level is provided by Android to EMM vendors via the mechanism used to enroll the agent. For example, to enroll a COPE device, the device has to be either a new device which has not been booted before or a factory resettled device. Hence, the user owned devices cannot be made as COPE as this requires a factory reset. Bellow diagram explains this distinction in more granular level.

![image](14.png)

Kiosks, as discussed here; belong to the **Dedicated device** category. A device dedicated to do a dedicated task(app) or a set of tasks(apps) is known as a dedicated device. Most common example is a Kiosk terminal such as an ATM or a vending machine that only runs one app and the user is not allowed to perform any other task. Similar to fully managed the devices, dedicated devices are owned by the admin and the enrollment is identical to a fully managed device. 


### Option 1: Side load agent

The `.apk` file for the Android agent can be obtained via the **Releases** tab in the Entgra Android Agent Gitlab repository.

It can be found [here](https://gitlab.com/entgra/android-agent/-/releases).
Proceed with the following steps once the agent is obtained and successfully stored in the device.

1. Make sure to allow app installations from the "Unknown sources" in settings.

    ![image](2.png)

2. Sideload the app `client-debug.apk` file from your preferred source and install.

    ![image](1.png)

### Option 2: Install agent via Play Store

1. Download the app from the Google **Play Store** and install via the standard Android app installation process. It should be noted however, that the agent that is obtained from the Play Store might perhaps be several versions behind the latest version.

{{< hint warning >}}
    It should be noted that sometimes the agent available on the Google Play Store 
    may not be the latest and might lack certain cutting edge features. 
    However utmost stability can be seen in the Play Store agent.
    If the bleeding edge agent is what you're looking for; refer Option 1 metioned above.
{{< /hint >}}

## Configuring the Server

Once the agent installation is complete, it is now required to configure the server to proceed with enrollment. 

1. Head to **Configuration Management** in the top menu. Then select **Platform Configurations** from the sub menu options.

    ![image](3.png)

    ![image](4.png)

2. Select **Android Configurations** in the next page to get the relevant options

    ![image](5.png)

3. Find the **Polling Interval** field and update accordingly. The next step would not work unless this is performed.

4. Scroll down until you come across the **SERVER_ADDRESS** field.
Here it is required to enter the IP address upon the server which Entgra IoTS is running.

![image](6.png)

Save and move onto the next step.
This concludes the preliminary server setup.

## Enrolling the Android Device

In order to successfully enroll a device on Entgra IoTS via QR, a series of steps need to be followed on the server end as well as the agent end.

This section discusses each of these sequentially.

### Server-side procedure

1. Go to **Device Management** in the top menu.
2. Go to **Enroll Device** to begin the enrollment process.
3. Select **Android** Device Type in the following screen.
4. The next screen will have several options that you can select depending on your enrollment process of choice. We shall be selecting the **Enroll using QR** option.

    ![image](7.png)

5. Select the ownership type as **COSU (KIOSK)** from the prompt that appears at the bottom of the page. This will generate a QR code.

    ![image](8.png)

This concludes the process to be followed on the server side.

### Agent procedure

1. In the first page of the Android agent you will encounter 2 options to enroll device. Of them, the **Enroll with QR** option should be selected.

    ![image](9.png)

2. This will launch a QR scanner within the app. The scanner should be used to scan the generated code in the Entgra IoTS enrollment page.

    ![image](11.png)

3. Successful scanning will result in the following page that explains a set of instructions to be followed.

    ![image](13.png)

4. As per instructions, it is now required that an `adb` command be run on the kiosk device to complete the enrollment.
The command is as follows;

        adb shell dpm set-device-owner io.entgra.iot.agent/org.wso2.iot.agent.services.AgentDeviceAdminReceiver

5. Once everything is successfully accomplished, the below screen will appear on the newly enrolled Android kiosk.

    ![image](12.png)





